import sys
import time
import Adafruit_DHT
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

sensor = Adafruit_DHT.DHT11
pin = 23


def readSensorDHC11():
    try:
        humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
        
        #devuelve un string con humedad,temperatura si ha podido leer bien
        return str(temperature)+","+str(humidity)
    except (Exception, e):
        print(str(e))
        #si ha habido error devuelve string vacio
        return ""





