import coordinate as Coordinate
import getData as data
import connection as con
import time
import json
from kafka import KafkaProducer
import random
import string
import nameGenerator as nameGenerator
import producer as prod
import requests
import sys
import coordinateBuilder
email ="bananas@bananas.com"
password= "Bananas123456$"
#array para coordenadas inventadas

dataLogin ={"email":email, "password":password}
print(dataLogin)
#array para uardar rutas
routes = []

#array para guardar los datos en caso de que no haya conexion
noConnectionData = []

#coordenadas inventadas añadidas al array
coordinates = []
if(len(sys.argv)==2):
        correctRute=False
        if(sys.argv[1]=="CostaRica-Munich"):
                 correctRute=True
                 print("Ruta CostaRica-Munich")
                 coordinates=coordinateBuilder.costaRicaMunich()
        if(sys.argv[1]=="Barcelona-Munich"):
                 correctRute=True
                 print("Barcelona-Munich")
                 coordinates=coordinateBuilder.barcelonaMunich()
        if(sys.argv[1]=="Vilanova-Munich"):
                 correctRute=True
                 print("Ruta Vilanova-Munich")
                 coordinates=coordinateBuilder.barcelonaMunich()
        if(sys.argv[1]=="Vilanova-Barcelona"):
                 correctRute=True
                 print("Ruta Vilanova-Barcelona")
                 coordinates=coordinateBuilder.vilanovaBarcelona()
        if(correctRute==False):
                 print("Ruta incorrecta cargando ruta por defecto... Ruta CostaRica-Munich")
                 coordinates=coordinateBuilder.costaRicaMunich()
else:
        print("Ruta no introducida cargando ruta por defecto... Ruta CostaRica-Munich")
        coordinates=coordinateBuilder.costaRicaMunich()


print(coordinates)

#añadimos las coordenadas de una ruta
routes.append(coordinates)
randomName=nameGenerator.randomName()
#llama al kafka 192.168.1.136 en puerto 9092

url="http://192.168.1.144:30502"
#autenticar y crear banana
result = requests.post(url= url+"/users/login", data=dataLogin).json()
user=result["user"]
token=user["accessToken"]
print(token)
result =requests.post(url=url+"/bananas/createBanana", data={"name":randomName}, headers={"Authorization":"Bearer "+str(token)}).json()
print(result)


#creamos la variable con el texto que se le va a enviar a kafka
stringToSend = ""
producer = prod.createConnection()

#recorre el array de coordenadas, en cuanto acabe se interpreta que ha llegado al destino
for coordinate in routes[random.randint(0,len(routes)-1)]:
    time.sleep(5)
    #raspberry1,15,100,41.2166666,1.7361208,2021-08-25
    #concatenamos las coordenadas con los datos de humedad y temperatura
    stringToSend =(randomName+","+str(data.readSensorDHC11())+","+str(coordinate.x)+","+str(coordinate.y)+","+time.strftime("%Y-%m-%d")).encode('utf-8')

#    stringToSend=json.dumps(stringToSend).encode('utf-8')
   #comprobamos que haya conexion
    if con.checkConnection():
        #si hay conexion, y hay datos previamente que no se han guardado por falta de conexion se enviaran
        if len(noConnectionData) >0:
            #recorremos el array de datos que no se han podido enviar
            for data in noConnectionData:
                #mandamos a kafka
                prod.sendMessage(producer, 'banana', stringToSend)
                print('bananas')
                print(stringToSend)
            #vaciamos el array de datos no mandados
            noConnectionData.clear()
        #mandamos datos recogidos cuando si habia conexion
        prod.sendMessage(producer, 'banana', stringToSend)
        print('bananas')
        print(stringToSend)
    else:
        #si no hay conexion se añade al array
        noConnectionData.append(stringToSend)



