from kafka import KafkaProducer

def createConnection():
    producer = KafkaProducer(bootstrap_servers='192.168.1.136:9092')
    return producer

def sendMessage(producer,topic, message):
    producer.send(topic, message)
